var heightYellowLine = $(".yellow-line> .yellow-line-inner");
var heightSectionInner = $(".copy-height");
function heightYellow(){
	heightYellowLine.height($(heightSectionInner).height() );
}

(function ($) {

	'use strict';

	// STICKY HEADER
	$(".header-sticky").sticky({
		topSpacing: 0,
	});

	// MAIN-NAVIGATION
	$('.main-navigation ul').superfish({
		delay: 400,
		animation: {
			opacity: 'show',
			height: 'show'
		},
		animationOut: {
			opacity: 'hide',
			height: 'hide'
		},
		speed: 200,
		speedOut: 200,
		autoArrows: false
	});

	// HOMEPAGE-CAROUSEL
	$('.homepage-carousel').owlCarousel({
		animateOut: 'fadeOut',
		items: 1,
		smartSpeed: 100,
		autoplay: false,
		autoplayTimeout: 3000,
		loop: true,
		nav: true,
		navText: [
			"<span class='arrow-left'></span>",
        	"<span class='arrow-right'></span>"
		],
	});

	// BRAND-CAROUSEL
	$('.brand-carousel').owlCarousel({
		items: 4,
		loop: true,
		nav: true,
		navText: [
			"<span class='arrow-left'></span>",
        	"<span class='arrow-right'></span>"
		],
		margin: 6,
		responsiveClass: true,
		responsive: {
			0:{
				items: 1
			},
			450: {
				items: 2
			},
			1000:{
				items: 4
			},
		}
	});

	$(window).on('load', function(){
		heightYellow();
	});

	// RUN FUNCTIONS ON WINDOW RESIZE
	$(window).on("resize", function() {
		heightYellow();
	});

	// CLONE AND APPEND TO RWD-NAV
	$(".main-navigation > ul").clone(false).find("ul,li").removeAttr("id").remove(".submenu").appendTo($(".nav-sidebar > ul"));

	$(".btn-rwd-main, .btn-hide").click( function(e){
		var nav_side_left = $("#nav-side-left");

		var wrapper = $(".wrapper-inner");

		nav_side_left.toggleClass("on-active");
		wrapper.toggleClass("on-active-left");
	});

})(jQuery); 